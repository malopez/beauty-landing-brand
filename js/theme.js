$(document)
    .ready(function () {
    //Carousel
    $('.owl-video-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: false,
        responsive: {
            0:{
                items:1
            },

            460:{
                items:2
            }, 

            767:{
                items:3
            }
        }
    })
    //initialize paroller.js
    $('[data-paroller-factor]').paroller();
    //Parallax
    $(".bg-slide-paroller").paroller({
        factor: 0.2
    });
    $(".bg-slide-paroller-small").paroller({
        factor: 0.1
    });
    $(".item-paroller-small").paroller({
        factor: 0.1,
        type: 'foreground'
    });
});
